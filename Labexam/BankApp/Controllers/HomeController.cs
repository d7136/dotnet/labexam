﻿using BankApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BankApp.Controllers
{
    public class HomeController : Controller
    {
       
            labexamEntities1 dbhelper = new labexamEntities1();

           [Authorize]
            public ActionResult Index()
            {
                return View("Index", dbhelper.UserTbs.ToList());
            }

            public ActionResult Create()
            {

                return View("Create");
            }

            public ActionResult AfterCreate(UserTb userToBeAdded)
            {
                dbhelper.UserTbs.Add(userToBeAdded);
                int rowsAffected = dbhelper.SaveChanges();
                if (rowsAffected > 0)
                {
                    return Redirect("/Home/Index");
                }
                else
                {
                    return View("Create");
                }
            }


        public ActionResult Delete(int id)
        {
            UserTb userToBeDeleted = dbhelper.UserTbs.Find(id);
            dbhelper.UserTbs.Remove(userToBeDeleted);

            int rowsAffected = dbhelper.SaveChanges();

            return Redirect("/Home/Index");//here we are giving instruction to the browser to shoot a new call to 
        }

        public ActionResult Edit(int id)
            {
            UserTb userToBeEdited = dbhelper.UserTbs.Find(id);

                return View("Edit", userToBeEdited);
            }

        public ActionResult AfterEdit(UserTb userStatusUpdated)
        {
            UserTb userStatusToBeEdited = dbhelper.UserTbs.Find(userStatusUpdated.UserNo);

            userStatusToBeEdited.Status = userStatusUpdated.Status;


            int rowsAffected = dbhelper.SaveChanges();
            if (rowsAffected > 0)
            {
                return Redirect("/Home/Index");
            }
            else
            {
                return View("Edit", userStatusUpdated); 
            }
        }

        public ActionResult SignIn()
        {
            return View("SignIn");
        }

        public ActionResult AfterSignIn(UserTb user, string ReturnUrl)
        {
            if (CheckWithDB(user))
            {
                FormsAuthentication.SetAuthCookie(user.Email, false);

                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.message = "UserName / Password is invalid";
                return View("SignIn");
            }
        }

        private bool CheckWithDB(UserTb user)
        {
            foreach (UserTb obj in dbhelper.UserTbs)
            {
                if (obj.Email == user.Email && obj.Password == user.Password)
                {
                    if (user.Role == "manager")
                    {
                        return true;
                    }
                    else { return false; }
                   
                }
            }
            return false;
        }


        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/SignIn");
        }
    }
}